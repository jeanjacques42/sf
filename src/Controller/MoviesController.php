<?php

namespace App\Controller;

use App\Entity\Movies;
use App\Form\MoviesType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;

class MoviesController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function new(Request $request)
    {
        $post = new Movies();
        $form = $this->createForm(MoviesType::class, $post);
        $form->add('submit', SubmitType::class, [
            'label' => 'Create',
            'attr' => ['class' => 'btn btn-default pull-right'],
        ]);
        return $this->render("form.html.twig", [
            'form' => $form->createView()
        ]);

    }
}
